from flask import Flask, render_template


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('config')

    # Load the default configuration
    app.config.from_object('config.default')

    # Load the configuration from the instance folder
    #app.config.from_pyfile('config.py')

    # Load the file specified by the APP_CONFIG_FILE environment variable
    # Variables defined here will override those in the default configuration
    #app.config.from_envvar('APP_CONFIG_FILE')

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/about')
    def about():
        return render_template('about.html')

    @app.route('/porridge')
    def porridge():
        return render_template('porridge.html')

    @app.route('/calculator')
    def calculator():
        return render_template('calculator.html')

    # endpoint
    @app.route('/get', methods=['GET'])
    def getData():
        #print(dir(app))
        print(*[f'{item}\n' for item in app.config.items()])
        #print(*[f'{item[0]}\n' for item in app.config.items()])
        return "OK", 200

    return app

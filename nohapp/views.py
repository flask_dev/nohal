"""Routes of application."""
from flask import Flask, render_template


def create_app():
    app = Flask(__name__)

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/about')
    def about():
        return render_template('about.html')

    @app.route('/porridge')
    def porridge():
        return render_template('porridge.html')

    @app.route('/calculator')
    def calculator():
        return render_template('calculator.html')

    @app.route('/disty_calc')
    def disty_calculator():
        """Tool for distillery calculation.
        """
        return render_template('disty_calc.html')

    return app
